# GoPhish + MailDev demo
Pour apprendre GoPhish, incluant un serveur de courriel MailDev

## Favoris
GoPhish: https://localhost:3333  
MailDev: http://localhost:1080

## Instructions
1. Démarrer les machines
```
docker compose up
```
2. Dans les logs, trouver le mot de passe pour gophish
```
gophish-gophish-1  | time="2023-04-19T12:36:30Z" level=info msg="Please login with the username admin and the password 2f59ff2b46bcacd3"
```
3. Se connecter à GoPhish avec le compte `admin` et le mot de passe trouvé précédemment (ex. `2f59ff2b46bcacd3`)

## Connecter GoPhish à MailDev
1. Dans GoPhish, naviguer dans *Sending Profiles*
2. Ajouter un nouveau profile avec les informations suivantes :

Paramètre|Valeur
--|--
Name|Default Sending Profile
SMTP From|*\<adresse courriel au choix\>*
Host|maildev:1025
3. Utiliser le bouton *Send Test Email* pour s'assurer que tout est fonctionnel avant d'enregistrer le profile
